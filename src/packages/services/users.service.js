const baseUrl = 'http://localhost:5000/users';

const getAll = () => {
  return fetch(baseUrl).then((res) => res.json());
};

const getOne = (id) => {
  const url = baseUrl + `/${id}`;
  return fetch(url).then((res) => res.json());
};

const post = (user) => {
  const url = baseUrl;
  return fetch(url, {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(user),
  }).then((res) => {
    return res.json();
  });
};

const remove = (id) => {
  const url = baseUrl + `/${id}`;
  return fetch(url, {
    method: 'DELETE', // *GET, POST, PUT, DELETE, etc.
  }).then((res) => {
    return res.json();
  });
};

const update = (user) => {
  const url = `${baseUrl}/${`${user.id}`}`;
  return fetch(url, {
    method: 'PUT',
    headers: new Headers({
      'Content-Type': 'application/json',
    }),
    body: JSON.stringify(user),
  }).then((res) => {
    return res.json();
  });
};

export { getAll, getOne, post, remove, update };
