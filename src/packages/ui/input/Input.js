import React from "react";
import "./input.css";

export const Input = (props) => {
  const { type, name, value, onChange } = props;
  return (
    <input min={0} type={type} name={name} value={value} onChange={onChange} />
  );
};
