import React from "react";
import "./button.css";

export const Button = (props) => {
  const { title, buttonStyle, handleClick } = props;
  return (
    <button className={buttonStyle} onClick={handleClick}>
      {title}
    </button>
  );
};
