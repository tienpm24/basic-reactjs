export const SET_CURRENT = 'SET_CURRENT';
export const UPDATE_USER = 'UPDATE_USER';
export const GET_USERS = 'GET_USERS';
export const ADD_USER = 'ADD_USER';
export const DELETE_USER = 'DELETE_USER';
export const USER_ERROR = 'USER_ERROR';
export const CLEAR_FORM_DATA = 'CLEAR_FORM_DATA';
