import {
  GET_USERS,
  SET_CURRENT,
  ADD_USER,
  UPDATE_USER,
  DELETE_USER,
  USER_ERROR,
  CLEAR_FORM_DATA,
} from '../types';

const inititalState = {
  users: null,
  current: null,
  error: null,
};

export default (state = inititalState, action) => {
  switch (action.type) {
    case GET_USERS:
      return {
        ...state,
        users: action.payload,
      };
    case ADD_USER:
      return {
        ...state,
        users: [...state.users, action.payload],
      };
    case UPDATE_USER:
      return {
        ...state,
        users: state.users.map((user) =>
          user.id === action.payload.id ? action.payload : user
        ),
      };

    case DELETE_USER:
      return {
        ...state,
        users: state.users.filter((user) => user.id !== action.payload),
      };
    case SET_CURRENT:
      return {
        ...state,
        current: action.payload,
      };

    case CLEAR_FORM_DATA:
      return {
        ...state,
        current: null,
      };
    case USER_ERROR:
      return {
        ...state,
        error: action.payload,
      };
    default:
      return state;
  }
};
