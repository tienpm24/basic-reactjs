import {
  GET_USERS,
  SET_CURRENT,
  ADD_USER,
  UPDATE_USER,
  DELETE_USER,
  USER_ERROR,
  CLEAR_FORM_DATA,
} from '../types';

export const getUsers = () => async (dispatch) => {
  try {
    const res = await fetch('/users');

    const data = await res.json();

    dispatch({
      type: GET_USERS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: USER_ERROR,
      payload: error.response.data,
    });
  }
};

export const addUser = (user) => async (dispatch) => {
  try {
    const res = await fetch('/users', {
      method: 'POST',
      body: JSON.stringify(user),
      headers: {
        'Content-Type': 'application/json',
      },
    });
    const data = await res.json();
    dispatch({
      type: ADD_USER,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: USER_ERROR,
      payload: error.response.data,
    });
  }
};

export const updateUser = (user) => async (dispatch) => {
  try {
    const res = await fetch(`/users/${user.id}`, {
      method: 'PUT',
      body: JSON.stringify(user),
      headers: {
        'Content-Type': 'application/json',
      },
    });

    const data = await res.json();

    dispatch({
      type: UPDATE_USER,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: USER_ERROR,
      payload: error.response.data,
    });
  }
};

export const deleteUser = (id) => async (dispatch) => {
  try {
    await fetch(`/users/${id}`, {
      method: 'DELETE',
    });

    dispatch({
      type: DELETE_USER,
      payload: id,
    });
  } catch (error) {
    dispatch({
      type: USER_ERROR,
      payload: error.response.data,
    });
  }
};

export const clearFormData = () => {
  return {
    type: CLEAR_FORM_DATA,
  };
};

export const setCurrentUser = (user) => {
  return {
    type: SET_CURRENT,
    payload: user,
  };
};
