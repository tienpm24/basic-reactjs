import React, { useEffect } from "react";
import { useState } from "react";
import { connect } from "react-redux";
import { Button } from "../../../../packages/ui/button/Button";
import { Input } from "../../../../packages/ui/input/Input";

import {
  addUser,
  updateUser,
  clearFormData,
} from "../../../redux/actions/userActions";

import "./FormUsers.css";

const FormUser = ({ currentUser, addUser, updateUser, clearFormData }) => {
  const [name, setName] = useState("");
  const [age, setAge] = useState(0);
  const [description, setDescription] = useState("");

  useEffect(() => {
    if (currentUser) {
      setName(currentUser.name);
      setDescription(currentUser.description);
      setAge(currentUser.age);
    } else {
      setName("");
      setDescription("");
      setAge(0);
    }
  }, [currentUser]);

  const onUpdateUser = () => {
    const updatedUser = {
      id: currentUser.id,
      name,
      age,
      description,
    };
    updateUser(updatedUser);
  };

  const onAddUser = () => {
    const newUser = {
      name,
      age,
      description,
    };
    addUser(newUser);
  };

  return (
    <div className="form-container">
      <label>Name</label>
      <Input
        type="text"
        name="name"
        value={name}
        onChange={(e) => setName(e.target.value)}
      />

      <label>Age</label>
      <Input
        type="number"
        name="age"
        value={age}
        onChange={(e) => setAge(e.target.value)}
      />

      <label>Description</label>
      <Input
        type="text"
        name="description"
        value={description}
        onChange={(e) => setDescription(e.target.value)}
      />
      <div className="actions">
        {currentUser ? (
          <Button
            title="Update"
            buttonStyle="btn-primary"
            handleClick={onUpdateUser}
          />
        ) : (
          <Button
            title="Submit"
            buttonStyle="btn-primary"
            handleClick={onAddUser}
          />
        )}
        <Button
          title="Cancel"
          buttonStyle="btn-stroke"
          handleClick={() => clearFormData()}
        />
      </div>
    </div>
  );
};
const mapStateToProps = (state) => ({
  currentUser: state.user.current,
});
export default connect(mapStateToProps, { updateUser, addUser, clearFormData })(
  FormUser
);
