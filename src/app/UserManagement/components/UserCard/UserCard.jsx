import React from "react";
import { connect } from "react-redux";
import { Button } from "../../../../packages/ui/button/Button";
import { setCurrentUser, deleteUser } from "../../../redux/actions/userActions";

const UserCard = ({ user, setCurrentUser, deleteUser }) => {
  const { id, name, age, description } = user;

  return (
    <li className="card" key={id}>
      <div onClick={() => setCurrentUser(user)}>
        <div className="image"></div>
        <h2>{name}</h2>
        <p>Age: {age}</p>
        <span className="description">{description}</span>
      </div>
      <div className="actions">
        <Button
          title="Remove"
          buttonStyle="btn-danger"
          handleClick={() => deleteUser(id)}
        />
      </div>
    </li>
  );
};

export default connect(null, { setCurrentUser, deleteUser })(UserCard);
