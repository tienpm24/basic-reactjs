import React, { useEffect } from "react";
import { connect } from "react-redux";
import FormUsers from "./components/FormUser/FormUsers";
import UserCard from "./components/UserCard/UserCard";
import { getUsers } from "../redux/actions/userActions";

import "../App.css";
import "./UserManagement.css";
import "../styles/custom/button.css";

const UserManagement = ({ users: { users }, getUsers }) => {
  useEffect(() => {
    getUsers();
  }, [getUsers]);

  return (
    <div className="container">
      <div className="left-container">
        <ul className="container">
          {!users ? (
            <p>No Users...</p>
          ) : (
            users.map((user, index) => (
              <UserCard key={index} user={user}></UserCard>
            ))
          )}{" "}
        </ul>
      </div>
      <div className="right-container">
        <FormUsers />
      </div>
    </div>
  );
};
const mapStateToProps = (state) => ({
  users: state.user,
});
export default connect(mapStateToProps, { getUsers })(UserManagement);
