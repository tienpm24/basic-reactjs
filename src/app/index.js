import React from "react";
import { Provider } from "react-redux";
import "./App.css";
import UserManagement from "./UserManagement/UserManagement.jsx";
import store from "./redux/store";

const App = () => {
  return (
    <Provider store={store}>
      <UserManagement />
    </Provider>
  );
};

export default App;
